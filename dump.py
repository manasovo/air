#!/usr/bin/env python

import json
import requests
import httplib2
import time
from datetime import datetime
import re
from termcolor import colored

COOKIE = ""
URL_COOKIE = "https://aisview.rlp.cz/"
URL_RLP = "https://aisview.rlp.cz/getData.php?"

LKTB_JSON = LKMT_JSON = LKPR_JSON = LKKV_JSON = ""

URL_LKTB = "cmd=tt&id=19001%2C3791%2C19016&"
URL_LKMT = "cmd=tt&id=18982%2C3886%2C19012&"
URL_LKPR = "cmd=tt&id=18991%2C3899%2C18993&"
URL_LKKV = "cmd=tt&id=18979%2C3844%2C18980&"

URL_LANG = "lang=1&"
URL_METEO = "is_meteo=0&"

date_from = "dt_od="
date_to = "dt_do="
date_end = "&"
time_utc = datetime.utcnow()
time_now = datetime.now()

time_from = int(time_utc.strftime("%Y%m%d%H%M%S"))
time_to = int(time_now.strftime("%Y%m%d%H%M%S"))

URL_DATE_OD = f"{date_from}{time_from}{date_end}"
URL_DATE_DO = f"{date_to}{time_to}{date_end}"

dummy = "dummy="
timenow = int(time.time() * 1000)
URL_DUMMY = f"{dummy}{timenow}"

NATO_WORDS = ["ALFA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIETT", "KILO", "LIMA", "MIKE", "NOVEMBER", "OSCAR", "PAPA", "QUEBEC", "ROMEO", "SIERRA", "TANGO", "UNIFORM", "VICTOR", "WHISKY", "X-RAY", "YANKEE", "ZULU"]
METAR_WORDS = ["METAR", "SPECI"]
METAR_WEATHER_WORDS = ["DRIZZLE", "RAIN", "SNOW", "ICE", "HAIL", "MIST", "UNKNOWN"]
METAR_CLOUD_WORDS = ["FEW", "BROKEN", "SCATTERED", "OVERCAST"]
METAR_STATUS = ["NOSIG", "TEMPO", "TREND"]

# DEBUG 1=ON, 0=OFF

DEBUG = 0


# ====== LIST OF FUNCTION ====== #

# check if rlp.cz servers are online
def check_server(server_url):
	if DEBUG == 1: print(colored("CHECK server", "green"))
	try:
		check = requests.get(server_url)
		if check.status_code == 200:
			if DEBUG == 1: print(colored("severs are ONLINE", "green"))
			return "ONLINE"
		else:
			if DEBUG == 1: print(colored("server ERROR", "red"))
			return "ERROR"
	except:
		return "server rlp.cz is not responding"


# get cookie from rlp.cz servers. If cookie exists use cached data.
def get_cookie():
	global COOKIE
	
	if len(COOKIE) == 0:
		session = requests.Session()
		response = session.get(URL_COOKIE)
		COOKIE = {'Cookie': response.headers['Set-Cookie']}
		if DEBUG == 1: print(colored("COOKIE get", "yellow"))
		return COOKIE
	else:
		if DEBUG == 1: print(colored("COOKIE cached", "green"))
		return COOKIE


# get data from rllp.cz servers
def get_data(airport_url):
	if DEBUG == 1: print(colored("GET data for ", "yellow") + airport_url)
	http = httplib2.Http()
	response, content = http.request(URL_RLP + airport_url + URL_LANG + URL_DATE_OD + URL_DATE_DO + URL_METEO + URL_DUMMY, 'GET', headers=get_cookie())
	return content


# get json for specific airport and save to variables
def get_json():
	if DEBUG == 1: print(colored("GET JSON", "blue"))
	global LKTB_JSON, LKMT_JSON, LKPR_JSON, LKKV_JSON
	
	LKTB_JSON = get_data(URL_LKTB)
	LKMT_JSON = get_data(URL_LKMT)
	LKPR_JSON = get_data(URL_LKPR)
	LKKV_JSON = get_data(URL_LKKV)


# get ATIS data from JSON
def get_atis(airport_json, id):
	atis = json.loads(airport_json)[id]['11_13']['data']['0']['txt']

	# check if ATIS have a data
	if len(atis) > 50:
		if DEBUG == 1: print(colored("GET ATIS data for ", "green") + id)
		return atis
	else:
		if DEBUG == 1: print(colored("ATIS data under lenght 50 for ", "red") + id)
		return False


# get name of airport from ATIS data
def airport_name(airport_atis):
	split = airport_atis.split("<br>")
	return split[0].split(' ')[2]														# print 3th world 0,1,2 = means 3 

# for search exact word only
def search_exact_word(word):
	return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search


# get ATIS information name
def search_info(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"					# security for EMPTY results
	if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" SEARCH nato name", "green"))

	info = []
	for word in NATO_WORDS:
		return_value = search_exact_word(word)(airport_atis)
		if return_value:
			info.append(word)
			return ' '.join(map(str, info))												# return only plain text without ['']

# get QNH number
def search_qnh(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"
	if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" SEARCH QNH number", "green"))
	return re.search(r'\d+', airport_atis.partition("QNH")[2]).group()


# get METAR data
def search_metar(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"
	if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" SEARCH METAR data", "green"))

	split = airport_atis.split("<br>")

	def metar_date():
		if any(word in str(split) for word in METAR_WORDS):	
			for date in split:
				if any(word in date.split() for word in METAR_WORDS):
					if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar DATE word found", "green"))
					return date + "<br>"
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no DATE word found", "red"))
			return ""
	
	def metar_wind():
		if any(word in str(split) for word in "WIND"):
			for wind in split:
				if "WIND" in wind:
					if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar WIND word found", "green"))
					return wind + "<br>"
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no WIND word found", "red"))
			return ""

	def metar_visibility():
		if any(word in str(split) for word in "VISIBILITY"):
			for visibility in split:
				if "VISIBILITY" in visibility:
					if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar VISIBILITY word found", "green"))
					return visibility + "<br>"
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no VISIBILITY word found", "red"))
			return ""
		
	def metar_weather():	
		for weather in split:
			if any(word in weather.split() for word in METAR_WEATHER_WORDS):
				if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar WEATHER word found", "green"))
				return weather + "<br>"
		
		if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no WEATHER word found", "red"))
		return ""

	def metar_clouds():
		for cloud in split:
			if any(word in cloud.split() for word in METAR_CLOUD_WORDS):
				if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar CLOUD word found", "green"))
				return cloud + "<br>"

		if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no CLOUD word found", "red"))
		return ""

	def metar_temp():
		if any(word in str(split) for word in "TEMPERATURE"):
			for temp in split:
				if "TEMPERATURE" in temp:
					if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar TEMPERATURE word found", "green"))
					return temp + "<br>"
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no TEMPERATURE word found", "red"))
			return ""

	def metar_dew():
		if any(word in str(split) for word in "DEWPOINT"):
			for dew in split:
				if "DEWPOINT" in dew:
					if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" metar DEWPOINT word found", "green"))
					return dew + "<br>"
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no DEWPOINT word found", "red"))
			return ""
	
	def metar_status():
		if any(word in str(split) for word in METAR_STATUS):	
			for status in split:
				if any(word in status.split() for word in METAR_STATUS):
					return status
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no STATUS word found", "red"))
			return ""
	
	return metar_date() + metar_wind() + metar_visibility() + metar_weather() + metar_clouds() + metar_temp() + metar_dew() + metar_status()


# get CONTACT data
def search_contact(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"
	if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" SEARCH contacts", "green"))

	split = airport_atis.split("<br>")
	pattern = re.compile(r'.*\b{}\b'.format("CONTACT"))
	contact = [x for x in split if pattern.match(x)]

	if len(contact) == 0: return ("NO CONTACT ISSUED")

	if len(contact) == 1:
		return ' '.join(map(str, contact))

	if len(contact) >= 2:																# if more than 1 contact do something
		contacts = []
		for text in contact:
			contacts.append(text + "<br>")
		return ' '.join(map(str, contacts))


# get RUNWAY data 
def search_rwy(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"

	split = airport_atis.split("<br>")

	def rwy_use():
		if DEBUG == 1: print(colored("RWY in use", "green"))
		if any(word in str(split) for word in "RUNWAY IN USE"):
			return re.search(r'\d+', airport_atis.partition("RUNWAY IN USE")[2]).group()
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no RUNWAY IN USE word found", "red"))
			return ""

	def rwy_status():
		if DEBUG == 1: print(colored("RWY status", "green"))
		exact_word = search_exact_word("RWY")(airport_atis)

		if not exact_word == None:
			pattern = re.compile(r'.*\b{}\b'.format("RWY"))
			rwy = [x for x in split if pattern.match(x)]

			if len(rwy) == 1:
				return "<br>" + ' '.join(map(str, rwy))

			if len(rwy) >= 2:
				rwy_status = []
				for text in rwy:
					rwy_status.append(text + "<br>")
				return "<br>" + ' '.join(map(str, rwy_status))
		else:
			if DEBUG == 1: print(colored(airport_name(airport_atis), "red") + colored(" no RWY word found", "red"))
			return ""

	return rwy_use() + rwy_status()


# get TRANSITION LEVEL data
def search_transition(airport_atis):
	if airport_atis is False: return "<center> no ATIS data </center>"
	if DEBUG == 1: print(colored(airport_name(airport_atis), "green") + colored(" SEARCH transition level", "green"))

	split = airport_atis.split("<br>")
	for transition in split:
		if "TRANSITION LEVEL" in transition:
			if len(transition) > 0:
				return transition
			else:
				return "error in transition"	


# get AIRPORT data as aditional info - popup
def airport_detail(airport_json, id):
	if DEBUG == 1: print(colored("SEARCH airport aditional data for ", "green") + id)

	airport_data = json.loads(airport_json)[id]['1_1']['data']

	airport_ident = airport_data['0']['txt'] + ": " + airport_data['0']['val'] + "\r\n------------------------------\r\n"
	airport_name = airport_data['1']['txt'] + ": " + airport_data['1']['val'] + "\r\n------------------------------\r\n"
	airport_elevation = airport_data['2']['txt'] + ": " + airport_data['2']['val'] + "\r\n------------------------------\r\n"
	airport_freq = airport_data['3']['txt'] + ": \n" + airport_data['3']['val'] + "\r\n------------------------------\r\n"
	airport_rwy = airport_data['4']['txt'] + ": \n" + airport_data['4']['val'] + "\r\n------------------------------\r\n"
	airport_icao = airport_data['5']['txt'] + ": " + airport_data['5']['val'] + "\r\n------------------------------\r\n"
	airport_atrib = airport_data['6']['txt'] + ": " + airport_data['6']['val']

	return airport_ident + airport_name + airport_elevation + airport_freq + airport_rwy + airport_icao + airport_atrib


def generate_html():
	if DEBUG == 1: print(colored("GENERATE HTML to standard output", "green"))

	#STYLE
	html_style = """\
	<style>
		h2 {
  		font-family: monospace;
  		font-weight: bold;
  		font-size: large;
  		color: #ff003d;}
 
		h4 {
  		font-family: monospace;
  		font-weight: 100;
  		font-size: medium;
  		color: #000;}

		.rwd-table {
  		font-family: monospace;
  		background: #fff;
  		color: #000;}

		.rwd-table th {
		font-family: monospace;
		font-weight: bold;
		font-size: medium;
		color: #ff003d;}

  		.rwd-table td {
		font-family: monospace;
		font-weight: 100;
		font-size: medium;
		color: #000;}
	</style>

	"""

	if DEBUG == 0:
		print(html_style)

	#GENERATE HTML TABLE
	gen_time = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
	check = check_server(URL_RLP)

	html = """\
	<html>
		<head>
			<meta charset="UTF-8">
		</head>
  		<body>
  			<h2>Czech Aeronautical Data - aisview.rlp.cz</h2>
			<h4>server rlp.cz: <b>{check}</b> generated at: <b>{gen_time}</b></h4>
			<table border "1" class="rwd-table">
				<tr><th></th>
					<th><span title="{lktb_airfield_detial}">LKTB BRNO/TUŘANY</span></th>
					<th><span title="{lkmt_airfield_detial}">LKMT OSTRAVA/MOŠNOV</span></th>
					<th><span title="{lkpr_airfield_detial}">LKPR PRAHA/RUZYNĚ</span></th>
					<th><span title="{lkkv_airfield_detial}">LKKV KARLOVY VARY</span></th>
				</tr>
			<tr><th>INFO</th><td>{lktb_info}</td><td>{lkmt_info}</td><td>{lkpr_info}</td><td>{lkkv_info}</td></tr>
			<tr><th>RWY</th><td>{lktb_rwy}</td><td>{lkmt_rwy}</td><td>{lkpr_rwy}</td><td>{lkkv_rwy}</td></tr>
			<tr><th>QNH</th><td>{lktb_qnh}</td><td>{lkmt_qnh}</td><td>{lkpr_qnh}</td><td>{lkkv_qnh}</td></tr>
			<tr><th>TL</th><td>{lktb_transition}</td><td>{lkmt_transition}</td><td>{lkpr_transition}</td><td>{lkkv_transition}</td></tr>
			<tr><th>CONTACT</th><td>{lktb_contact}</td><td>{lkmt_contact}</td><td>{lkpr_contact}</td><td>{lkkv_contact}</td></tr>
			<tr><th>METAR</th><td>{lktb_metar}</td><td>{lkmt_metar}</td><td>{lkpr_metar}</td><td>{lkkv_metar}</td></tr>
			</table>
  		</body>
	</html>
	""".format(gen_time=gen_time, check=check,
							lktb_airfield_detial = lktb_airfield_detial, lkmt_airfield_detial = lkmt_airfield_detial, lkpr_airfield_detial = lkpr_airfield_detial, lkkv_airfield_detial = lkkv_airfield_detial,
							lktb_info=lktb_info, lkmt_info=lkmt_info, lkpr_info=lkpr_info, lkkv_info=lkkv_info,
							lktb_rwy=lktb_rwy, lkmt_rwy=lkmt_rwy, lkpr_rwy=lkpr_rwy, lkkv_rwy=lkkv_rwy,
							lktb_transition=lktb_transition, lkmt_transition=lkmt_transition, lkpr_transition=lkpr_transition, lkkv_transition=lkkv_transition,
							lktb_contact=lktb_contact, lkmt_contact=lkmt_contact, lkpr_contact=lkpr_contact, lkkv_contact=lkkv_contact,
							lktb_qnh=lktb_qnh, lkmt_qnh=lkmt_qnh, lkpr_qnh=lkpr_qnh, lkkv_qnh=lkkv_qnh,
							lktb_metar=lktb_metar, lkmt_metar=lkmt_metar, lkpr_metar=lkpr_metar,lkkv_metar=lkkv_metar)
	if DEBUG == 0:
		print(html)



# ===== GO FOR IT BABY ===== "
if check_server(URL_RLP) == "ONLINE":

	# if servers are online get all data to json
	get_json()


	# parse data for LKTB
	if DEBUG == 1: print(colored("===== LKTB ATIS parsing started =====", "blue"))
	ATIS = get_atis(LKTB_JSON, "3791")

	lktb_info = search_info(ATIS)
	lktb_rwy = search_rwy(ATIS)												# search first integer from RWY string in text plus RWY info
	lktb_transition = search_transition(ATIS)	
	lktb_contact = search_contact(ATIS)										# search all lines with word CONTACT
	lktb_qnh = search_qnh(ATIS)												# search first integer from QNH string in text
	lktb_metar = search_metar(ATIS)
	lktb_airfield_detial = airport_detail(LKTB_JSON, "3791")

	# parse data for LKMT
	if DEBUG == 1: print(colored("===== LKMT ATIS parsing started =====", "blue"))
	ATIS = get_atis(LKMT_JSON, "3886")

	lkmt_info = search_info(ATIS)
	lkmt_rwy = search_rwy(ATIS)
	lkmt_transition = search_transition(ATIS)
	lkmt_contact = search_contact(ATIS)			
	lkmt_qnh = search_qnh(ATIS)
	lkmt_metar = search_metar(ATIS)
	lkmt_airfield_detial = airport_detail(LKMT_JSON, "3886")

	# parse data for LKPR
	if DEBUG == 1: print(colored("===== LKPR ATIS parsing started =====", "blue"))
	ATIS = get_atis(LKPR_JSON, "3899")

	lkpr_info = search_info(ATIS)
	lkpr_rwy = search_rwy(ATIS)
	lkpr_transition = search_transition(ATIS)
	lkpr_contact = search_contact(ATIS)
	lkpr_qnh = search_qnh(ATIS)
	lkpr_metar = search_metar(ATIS)
	lkpr_airfield_detial = airport_detail(LKPR_JSON, "3899")

	# parse data for LKKV
	if DEBUG == 1: print(colored("===== LKKV ATIS parsing started =====", "blue"))
	ATIS = get_atis(LKKV_JSON, "3844")

	lkkv_info = search_info(ATIS)
	lkkv_rwy = search_rwy(ATIS)
	lkkv_transition = search_transition(ATIS)
	lkkv_contact = search_contact(ATIS)
	lkkv_qnh = search_qnh(ATIS)
	lkkv_metar = search_metar(ATIS)
	lkkv_airfield_detial = airport_detail(LKKV_JSON, "3844")

	# final generate a html web as stdout
	generate_html()

# if servers are offline
else:
	STATUS = "<center>" + check_server(URL_RLP) + "</center>"

	lktb_airfield_detial = lkmt_airfield_detial = lkpr_airfield_detial = lkkv_airfield_detial = STATUS
	lktb_info = lkmt_info = lkpr_info = lkkv_info = STATUS
	lktb_rwy = lkmt_rwy = lkpr_rwy = lkkv_rwy = STATUS
	lktb_qnh = lkmt_qnh = lkpr_qnh = lkkv_qnh  = STATUS
	lktb_transition = lkmt_transition = lkpr_transition = lkkv_transition = STATUS
	lktb_contact = lkmt_contact = lkpr_contact = lkkv_contact = STATUS
	lktb_metar = lkmt_metar = lkpr_metar = lkkv_metar = STATUS
	
	# final generate a html web as stdout
	generate_html()
